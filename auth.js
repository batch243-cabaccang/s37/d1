require("dotenv").config();

const jwt = require("jsonwebtoken");

const authorization = {
    authToken: (user) => {
        const authToken = jwt.sign(user.toJSON(), process.env.TOKEN);
        return authToken;
    },

    verify: (req, res, next) => {
        const token = req.headers.authorization.split(" ")[1];
        if (!token) {
            res.json({ message: "No Token" });
            return;
        }
        jwt.verify(token, process.env.TOKEN, (error) => {
            if (error) {
                res.json({ error: error.message });
                return;
            }
            next();
        });
    },
    decode: (token) => {
        if (!token) {
            return null;
        }
        token = token.split(" ")[1];
        try {
            jwt.verify(token, process.env.TOKEN);
            return jwt.decode(token, { complete: true }).payload;
        } catch (error) {
            res.json({ message: error.message });
        }
    },
};

module.exports = authorization;
