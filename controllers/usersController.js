const User = require("../models/usersModel");
const bcrypt = require("bcrypt");
const Course = require("../models/coursesModel");
const authorization = require("../auth");

module.exports.checkEmailExists = async (req, res, next) => {
    const users = await User.find({ email: req.body.email });
    try {
        if (!users.length == 0) {
            res.json({ message: "Email Taken" });
            return;
        }
        next();
    } catch (error) {
        res.json({ message: error.message });
    }
};
module.exports.creatUser = async (req, res) => {
    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPassword,
        mobileNo: req.body.mobileNo,
    });
    try {
        await newUser.save();
        res.json(newUser);
    } catch (error) {
        res.json({ message: error.message });
    }
};
module.exports.loginUser = async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
        res.json({ message: "Email is not registered." });
        return;
    }
    try {
        const isMatch = await bcrypt.compare(req.body.password, user.password);
        if (!isMatch) {
            res.json({ message: "Incorrect Password" });
            return;
        }
        const accessToken = authorization.authToken(user);
        console.log(accessToken);
        res.json(accessToken);
    } catch (error) {
        res.json({ error: error.message });
    }
};
//   ACTIVTY CODES
module.exports.getUserById = async (req, res) => {
    try {
        let user = await User.findById(req.body.id);
        user.password = "";
        if (!user) {
            res.status(404);
            return res.json({ message: "User not Found" });
        }
        res.json(user);
    } catch (error) {
        res.status(500);
        return res.json({ message: error.message });
    }
};
// ------------

module.exports.profileDetails = async (req, res) => {
    const userData = authorization.decode(req.headers.authorization);

    try {
        const user = await User.findById(userData._id);
        user.password = "Confidential";
        return res.send(user);
    } catch (error) {
        res.json({ message: error.message });
    }
};

module.exports.updateRole = async (req, res) => {
    const token = req.headers.authorization;
    const userData = authorization.decode(token);
    const idOfUserToBeUpdated = req.params.userId;

    if (!userData.isAdmin) return res.send("You ain't no Admin, B****!");

    try {
        const userTobeUpdated = await User.findById(idOfUserToBeUpdated);

        const changeRole = {
            isAdmin: !userTobeUpdated.isAdmin,
        };

        const updateUser = await User.findByIdAndUpdate(
            idOfUserToBeUpdated,
            changeRole,
            { new: true }
        );
        res.send(updateUser);
    } catch (error) {
        res.send({ message: error.message });
    }
};

module.exports.enroll = async (req, res) => {
    const token = req.headers.authorization;
    const userData = authorization.decode(token);
    const courseId = req.params.courseId;

    if (userData.isAdmin) return res.send("Admin ka eh.");

    try {
        const course = await Course.findById(courseId);
        if (!course) return res.send("Anong Course Yan?");
        course.enrollees.push({ userId: userData._id });
        course.slots -= 1;
        await course.save();
        // console.log(course);

        const user = await User.findById(userData._id);
        if (!user) return res.send("Sino Mag-eenroll?");
        user.enrollments.push({ courseId: course.id });
        await user.save();
        // console.log(user);

        res.send("Okay na. Bayaran mo na lang, ha?");
    } catch (error) {
        res.json({ message: error.message });
    }
};
