const Course = require("../models/coursesModel");
const authorization = require("../auth");
const coursesModel = require("../models/coursesModel");

module.exports.addCourse = async (req, res) => {
    const userData = authorization.decode(req.headers.authorization);
    console.log(userData);
    if (!userData.isAdmin) return res.send("Not an Admin");

    const newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots,
    });

    try {
        const addedCourse = await newCourse.save();
        console.log(addedCourse);
        res.send(true);
    } catch (error) {
        console.log({ message: error.message });
        res.send(false);
    }
};

module.exports.getAllActive = async (req, res) => {
    try {
        const activeCourses = await Course.find({ isActive: true });
        res.send(activeCourses);
    } catch (error) {
        res.send({ message: error.message });
    }
};

module.exports.getCourse = async (req, res) => {
    const courseId = req.params.courseId;
    console.log(courseId);
    try {
        const course = await Course.findById(courseId);
        res.send({ course });
    } catch (error) {
        res.send({ message: error.message });
    }
};

module.exports.updateCourse = async (req, res) => {
    const userData = authorization.decode(req.headers.authorization);
    console.log(userData);
    if (!userData.isAdmin) return res.send("You have not right!");
    let updatedCourse = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots,
    };

    const courseId = req.params.courseId;
    try {
        const theUpdatedCourse = await Course.findByIdAndUpdate(
            courseId,
            updatedCourse,
            { new: true }
        );
        res.json({ theUpdatedCourse });
    } catch (error) {
        res.send({ message: error.message });
    }
};

module.exports.archiveOrRetrieveCourse = async (req, res) => {
    const userData = authorization.decode(req.headers.authorization);
    console.log(userData);
    if (!userData.isAdmin) return res.send("You are not an Admin.");

    let changeIsActive = {
        isActive: req.body.isActive,
    };

    const courseId = req.params.courseId;
    try {
        const archivedOrRetrievedCourse = await Course.findByIdAndUpdate(
            courseId,
            changeIsActive,
            { new: true }
        );
        console.log(archivedOrRetrievedCourse);
        res.send(true);
    } catch (error) {
        console.log({ message: error.message });
        res.send(false);
    }
};

module.exports.getAllCourses = async (req, res) => {
    const token = req.headers.authorization;
    const userData = authorization.decode(token);
    console.log(userData);

    if (!userData.isAdmin) return res.send("Oh, no you don't.");
    try {
        const courses = await Course.find();
        res.json(courses);
    } catch (error) {
        res.send({ message: error.message });
    }
};

