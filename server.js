require("dotenv").config();

const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const port = process.env.PORT;

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error.message));
db.once("open", () => console.log("Connected to DB"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

const courseRoutes = require("./routes/courseRoutes");
app.use("/courses", courseRoutes);

app.listen(port, console.log(`Server running on Port ${port}`));
