const express = require("express");
const userRoutes = express.Router();
const userControllers = require("../controllers/usersController");
const authorization = require("../auth");

userRoutes.get(
    "/checkUser",
    authorization.verify,
    userControllers.checkEmailExists
);
userRoutes.post(
    "/createUser",
    userControllers.checkEmailExists,
    userControllers.creatUser
);
userRoutes.post("/login", userControllers.loginUser);
userRoutes.post("/getUser", authorization.verify, userControllers.getUserById);
userRoutes.get("/profile", userControllers.profileDetails);

userRoutes.patch(
    "/updateRole/:userId",
    authorization.verify,
    userControllers.updateRole
);

userRoutes.post(
    "/enroll/:courseId",
    authorization.verify,
    userControllers.enroll
);

module.exports = userRoutes;
