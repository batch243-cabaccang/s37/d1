const express = require("express");
const courseRouter = express.Router();
const courseController = require("../controllers/courseController");
const authorization = require("../auth");

courseRouter.post("/", authorization.verify, courseController.addCourse);

courseRouter.get("/allActiveCourses", courseController.getAllActive);

courseRouter.get(
    "/allCourses",
    authorization.verify,
    courseController.getAllCourses
);

courseRouter.get("/:courseId", courseController.getCourse);

courseRouter.put(
    "/update/:courseId",
    authorization.verify,
    courseController.updateCourse
);

courseRouter.patch(
    "/:courseId/archive",
    authorization.verify,
    courseController.archiveOrRetrieveCourse
);

module.exports = courseRouter;
