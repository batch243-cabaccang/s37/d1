const mongoose = require("mongoose");

const typeString = {
    type: String,
    required: true,
};

const courseSchema = new mongoose.Schema({
    name: typeString,
    description: typeString,
    price: {
        type: Number,
        required: [true, "Price is Required"],
    },
    slots: {
        type: Number,
        required: [true, "Slot is Required"],
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
    enrollees: [
        {
            userId: {
                type: String,
                required: [true, "User ID is Required"],
            },
            enrolledOn: {
                type: Date,
                default: new Date(),
            },
        },
    ],
});

module.exports = mongoose.model("Courses", courseSchema);
